<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class Client extends Model
{
	const DEFAULT_PAGE_SIZE = 5;
	protected $table = 'client';
	
	public $timestamps = false;
	
	public static function search($criteria) {
		// DB::enableQueryLog();
		
		if ( empty($criteria) ) return [];
		if ( !isset($criteria['q']) || strlen($criteria['q']) < 2 ) return [];

		$page = isset($criteria['page']) ? intval($criteria['page']) : 1;
		if ( $page <=0 ) $oage = 1;

		$page_size = isset($criteria['page_size']) ? intval($criteria['page_size']) : self::DEFAULT_PAGE_SIZE;
		if ( $page_size <=0 ) $oage = self::DEFAULT_PAGE_SIZE;
		
		$phrase = $criteria['q'];
		$query = self::where(function ($query) use ($phrase) {
			$query->where('first_name', 'LIKE', $phrase . '%' );
			$query->orWhere('last_name', 'LIKE', $phrase . '%' );
			$query->orWhere('phone_search', 'LIKE', $phrase . '%' );
			$query->orWhere('phone', 'LIKE', $phrase . '%' );
		});		
		if (array_key_exists('days_expiring_within', $criteria)) {
			$expiring_date = strtotime('+' . $criteria['days_expiring_within'] . ' days');
			$query->where('membership_type', '<>', 'Guest' );
			$query->where('membership_expiry_date', '<=', date('Y-m-d', $expiring_date) );
			$query->where('membership_expiry_date', '>', date('Y-m-d') );
		}
		
		if (array_key_exists('sort_order', $criteria)) {
			$sortOrder = $criteria['sort_order'];
			if ($sortOrder == 'last_name') {
				$query->orderBy('last_name');
			} else if ($sortOrder == 'last_name_desc') {
				$query->orderBy('last_name', 'desc');
			} else {
				$query->orderBy('last_name');
			}
		}
		
		$result = array();
		$record_total = $query->count();
		$result['current_page'] = $page;
		$result['page_size'] = $page_size;
		$result['record_total'] = $record_total;
		$result['page_total'] = ($record_total % $page_size) == 0 ? $record_total/$page_size : round($result['record_total'] / $page_size + 0.5);

		$clients = $query->skip(($page-1)*$page_size)->take($page_size)->get();
		$now = time();
		foreach($clients as $client) {
			$seconds_to_expire = strtotime($client->membership_expiry_date) - $now;
			if ($seconds_to_expire == 0) {
				$client->days_to_expiration = 0;
			} else {
				$client->days_to_expiration = round($seconds_to_expire/(3600*24) + 0.5);
			}
		}
		$result['page_records'] = $clients;
		
		// dd(DB::getQueryLog());
		
		return $result;
	}
}
