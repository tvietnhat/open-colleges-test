<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Client;

class ApiController extends Controller {
	public static function searchClient(Request $request) {
		$criteria = $request->all();
		$clients = Client::search($criteria);
		return response()->json($clients);
	}
}
