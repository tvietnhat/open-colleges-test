// Register `clientSearch` component, along with its associated controller and template
angular.
  module('clientSearch').
  component('clientSearch', {
    templateUrl: 'client-search/client-search.html',
    controller: function ClientSearchController( $http, $timeout ) {
		this.requestedPage = 1;
		this.membershipExpiringIn30Days = false;
		
		var self = this;
		this.doSearch = function () {
			self.errorMessage = null;
			self.infoMessage = null;
			self.clients = null;
			self.searchResult = null;
			
			if (self.query == undefined || self.query.length < 2) {
				self.errorMessage = 'Search phrase must have at least 2 characters.';
				return;
			}
			
			self.isSearching = true;
			var requestUrl = '/api/client/search?q=' + encodeURIComponent(self.query);
			if (self.requestedPage > 1) {
				requestUrl += '&page=' + encodeURIComponent(self.requestedPage);
			}
			if (self.membershipExpiringIn30Days) {
				requestUrl += '&days_expiring_within=30';
			}
			$http.get(requestUrl).then(function(response) {
				self.isSearching = false;
				self.searchResult = response.data;
				if (!self.searchResult) {
					self.errorMessage = 'Something wrong';
					return;
				}
				self.clients = self.searchResult.page_records; 
				if (self.searchResult.page_records.length == 0) {
					self.infoMessage = 'No result found for "' + self.query + '"';
				}
			});
		};
		
		this.queryInputKeyup = function(e){
			if (e.keyCode == 13) {
				self.doSearch();
			}
		};
		
		this.pageChange = function(page) {
			self.requestedPage = page;
			self.doSearch();
		}
		
		this.queryChange = function() {
			self.requestedPage = 1;
		}
    }
  });
