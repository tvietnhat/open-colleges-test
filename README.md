# open-colleges-test

## Installation and Config
+ The app uses PHP [Lavarel 5.2](https://laravel.com/docs/5.2/) for the backend. Follow their documentation for configuration instruction
+ Use [Composer](https://getcomposer.org) to install PHP dependency packages
```bash
foo@bar:~$ composer install
```
+ Sample Database script (MySQL) can be found [here](sample-database.sql)

