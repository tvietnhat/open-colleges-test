<!doctype html>
<html lang="en" ng-app="ocApp">
  <head>
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
    <title>Open Colleges</title>
	
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">
	
	<link rel="stylesheet" href="css/app.css" crossorigin="anonymous">

    <script src="/vendor/angular/angular.min.js"></script>
    <script src="app.module.js"></script>
    <script src="client-search/client-search.module.js"></script>
    <script src="client-search/client-search.component.js"></script>
  </head>
  <body>
	  <!-- <nav class="navbar navbar-default navbar-fixed-top">
	    <div class="container">
	      ...
	    </div>
	  </nav> -->
	 <div class="container">
		 <div class="rơw">
		 	<div class="col-sm-12">
		 		<h1>Search Client</h1>
		 	</div>
		 </div>
		 <client-search></client-search>
		    
	 </div>

  </body>
</html>